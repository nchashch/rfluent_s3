#![allow(non_snake_case)]
extern crate libc;
extern crate rmpv;
extern crate rusoto_core;
extern crate rusoto_s3;
extern crate chrono;
extern crate fluent_bit_rust;

use rusoto_core::Region;
use rusoto_core::credential::EnvironmentProvider;
use rusoto_core::request::HttpClient;
use rusoto_s3::{S3Client, S3, PutObjectRequest, StreamingBody};

use fluent_bit_rust::output;
use std::env;

#[no_mangle]
unsafe fn FLBPluginRegister(def: *mut output::FLBPluginProxyDef) -> libc::c_int {
	  return output::FLBPluginRegister(def, "rfluent_s3", "StdOut.rs");
}

static mut CLIENT: Option<S3Client> = None;

#[no_mangle]
unsafe fn FLBPluginInit(_plugin: *mut output::FLBOutPlugin) -> libc::c_int {
    let request_dispatcher = HttpClient::new().unwrap();
    let credentials_provider = EnvironmentProvider::default();
    let region = Region::default();
    CLIENT = Some(S3Client::new_with(request_dispatcher,
                                credentials_provider,
                                region));
    match &CLIENT {
        Some(client) => {
            let buckets = client.list_buckets().sync();
            println!("{:#?}", buckets);
        },
        None => panic!("Couldn't create S3 client"),
    };
	  return output::FLB_OK;
}

#[no_mangle]
unsafe fn FLBPluginFlush(data: *const u8, length: libc::c_int, _tag: *const libc::c_char) -> libc::c_int {
    println!();
    let data_u8: &[u8] = std::slice::from_raw_parts(data, length as usize);
    let mut data_cursor = std::io::Cursor::new(data_u8);
    let record = rmpv::decode::read_value(&mut data_cursor).unwrap();
    let record = record.as_array().unwrap();
    // let ext = &record.as_slice()[0];
    let map = &record.as_slice()[1];
    let map = map.as_map().unwrap();
    let now: chrono::DateTime<chrono::Utc> = chrono::Utc::now();
    println!("{}", now.to_rfc2822());
    for (k, v) in map {
        println!("{}: {}", k, v);
    }
    let bucket: String = match env::var_os("AWS_S3_BUCKET") {
        Some(val) => val.into_string().unwrap(),
        None => panic!("AWS_S3_BUCKET environment variable was not set"),
    };
    match &CLIENT {
        Some(client) => {
            let byte_stream = StreamingBody::from(data_u8.to_owned());
            client.put_object(PutObjectRequest {
                bucket: bucket,
                key: now.to_rfc2822(),
                body: Some(byte_stream.into()),
                ..Default::default()
            }).sync().expect("could not upload");
            println!("Object written to S3.");
        },
        None => panic!("Couldn't create S3 client"),
    };
	  return output::FLB_OK;
}

#[no_mangle]
fn FLBPluginExit() -> libc::c_int {
	  return output::FLB_OK;
}

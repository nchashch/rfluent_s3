# Usage
Before running the example you need to export environment variables.
```bash
export AWS_ACCESS_KEY_ID=YOURACCESSKEYID &&
export AWS_SECRET_ACCESS_KEY=yoursecret &&
export AWS_REGION=eu-central-1 (your region) &&
export AWS_S3_BUCKET=name-of-your-bucket
```
After environment variables have been exported, you can run the example like this:
```bash
bin/fluent-bit -i cpu -e /path/to/rfluent_s3/target/release/librfluent_s3.so -o rfluent_s
```
provided you have already built `fluent-bit` with
```bash
cd build/ &&
cmake -DFLB_DEBUG=On -DFLB_PROXY_GO=On ../ &&
make
```
When run the example will print incoming data to `stdout` and create an S3 object
for every call to `FLBPluginFlush` with the key set to current UTC datetime
in a human readable format, and the body set to raw MessagePack data.
